package ru.khusnullin.taskmanager.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String title;

    @ManyToOne
    private User owner;

    @OneToMany(mappedBy = "project")
    private List<Task> tasks;

    @ManyToOne
    private Project parent;

    //todo почитать про hibernate tree model https://habr.com/ru/articles/537062/
    @OneToMany(mappedBy = "parent")
    private List<Project> children;

    public void addChild(Project child) {
        if (child.isAncestorOf(this)) {
            throw new IllegalArgumentException("Parent project cannot be a descendant of the child project");
        }
        children.add(child);
        child.setParent(this);
    }

    private boolean isAncestorOf(Project project) {
        Project parent = project.getParent();
        while (parent != null) {
            if (parent == this) {
                return true;
            }
            parent = parent.getParent();
        }
        return false;
    }
}

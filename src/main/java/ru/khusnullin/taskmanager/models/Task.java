package ru.khusnullin.taskmanager.models;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor(force = true)
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String title;

    @ManyToOne
    private User owner;

    @ManyToOne
    private Project project;

    @Column(updatable = false)
    private LocalDateTime createdTime;

    private LocalDateTime statusChangeTime;

    public enum TaskStatus {
        NEW, IN_PROGRESS, DONE
    }

    @JsonIgnore
    @Transient
    private TaskStatus previousStatus;

    private TaskStatus taskStatus;

    private String commonInformation;

    public enum Type {
        TECH, MANAGER
    }

    @Column(updatable = false)
    @NotNull
    private Type type;

    @PrePersist
    protected void onCreate() {
        createdTime = LocalDateTime.now();
        taskStatus = TaskStatus.NEW;
        if(type == Type.TECH) {
            commonInformation = "Information for TECH specialists";
        } else {
            commonInformation = "This is common information for MANAGER";
        }
    }

    @PreUpdate
    protected void onUpdate() {
        if(previousStatus != taskStatus) {
            statusChangeTime = LocalDateTime.now();
        }
        previousStatus = taskStatus;
    }
}

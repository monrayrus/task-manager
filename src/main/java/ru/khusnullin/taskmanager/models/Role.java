package ru.khusnullin.taskmanager.models;


public enum Role {
    USER, ADMIN
}

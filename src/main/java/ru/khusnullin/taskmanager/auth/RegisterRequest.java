package ru.khusnullin.taskmanager.auth;

import lombok.*;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class RegisterRequest {
    private String username;
    private String password;
}

package ru.khusnullin.taskmanager.services;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Claims;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.function.Function;


@Service
public class JwtService {

    //https://youtu.be/KxqlJblhzfI?t=3475
    private static final String SECRET_KEY = "244226452948404D635166546A576E5A7234753778217A25432A462D4A614E64";

    //извлекает имя пользователя (subject) из JWT токена.
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    //извлекает произвольный плейнтекст (claim) из JWT токена, используя переданную функцию для извлечения нужного claim'а.
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    //извлекает все claim'ы из JWT токена.
    public Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    //возвращает ключ для подписи JWT токенов, используя секретный ключ, хранящийся в поле SECRET_KEY.
    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }
    //генерирует JWT токен на основе переданных пользовательских данных, при этом не добавляет дополнительных claim'ов.
    public String generateToken(UserDetails userDetails) {
        return generateToken(new HashMap<>(), userDetails);
    }

    //генерирует JWT токен на основе переданных пользовательских данных и дополнительных claim'ов, переданных в виде Map.
    public String generateToken(HashMap<String, Object> claims, UserDetails userDetails) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date((System.currentTimeMillis() + 1000 * 60 * 24)))
                .signWith(getSigningKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }
    //извлекает время истечения токена
    private Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }
}

package ru.khusnullin.taskmanager.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.khusnullin.taskmanager.models.Project;
import ru.khusnullin.taskmanager.models.Task;
import ru.khusnullin.taskmanager.services.ProjectService;
import ru.khusnullin.taskmanager.services.TaskService;

import javax.annotation.security.PermitAll;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/tasks/")
public class TaskController {

    private final TaskService taskService;
    private final ProjectService projectService;

    //Все пользователи
    @GetMapping("/")
    public List<Task> getAllTasks() {
        return taskService.getAllTasks();
    }
    //Все пользователи
    @GetMapping("/{id}")
    public ResponseEntity<Task> getTaskById(@PathVariable Long id) {
        Optional<Task> task = taskService.getTaskById(id);
        return task.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    //Все пользователи
    @PostMapping("/")
    public ResponseEntity<Task> createTask(@RequestBody Task task) {
        Task savedTask = taskService.saveTask(task);
        return ResponseEntity.ok(savedTask);
    }
    //Только админ
    @PutMapping("/{id}")
    public ResponseEntity<Task> updateTask(@PathVariable Long id, @RequestBody Task task) {
        Optional<Task> existingTask = taskService.getTaskById(id);
        if(existingTask.isPresent()) {
            task.setId(id);
            Task updatedTask = taskService.saveTask(task);
            return ResponseEntity.ok(updatedTask);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    //Только админ
    //<Void>?
    @DeleteMapping("/{id}")
    public ResponseEntity<Task> deleteTask(@PathVariable Long id) {
        Optional<Task> existingTask = taskService.getTaskById(id);
        if(existingTask.isPresent()) {
            taskService.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}/project")
    public ResponseEntity<Project> getProjectByTask(@PathVariable Long id) {
        Optional<Task> task = taskService.getTaskById(id);
        if(task.isPresent()) {
            Optional<Project> project = projectService.getProjectByTask(task.get());
            if (project.isPresent()) return ResponseEntity.ok(project.get());
        }
        return ResponseEntity.notFound().build();
    }


}

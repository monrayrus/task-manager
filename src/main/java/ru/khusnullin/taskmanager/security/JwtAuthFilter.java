package ru.khusnullin.taskmanager.security;

import com.sun.istack.NotNull;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.khusnullin.taskmanager.services.JwtService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {

    private final JwtService jwtService;

    private final UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain filterChain) throws ServletException, IOException {
        final String authHeader = request.getHeader("Authorization");
        final String jwt;
        final String username;
        if(authHeader == null || !authHeader.startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
            return;
        }

        jwt = authHeader.substring(7);
        username = jwtService.extractUsername(jwt);
        //проверяется наличие аутентификации пользователя и отсутствие уже установленной аутентификации в контексте безопасности приложения.
        if(username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

            /*
            Если токен валиден, то создается объект UsernamePasswordAuthenticationToken с передачей в конструктор userDetails -
            это объект класса, который содержит информацию о пользователе, включая его имя пользователя, пароль и список его ролей.
            Затем вызывается метод setDetails у созданного объекта authToken, который устанавливает дополнительные детали аутентификации,
            такие как IP-адрес, с которого был сделан запрос, время запроса и т.д.
            Наконец, объект authToken устанавливается как текущая аутентификация в контексте безопасности приложения с помощью метода setAuthentication
            у объекта SecurityContextHolder.getContext(). Это позволяет сохранить информацию об аутентификации пользователя в течение дальнейшей работы приложения,
            чтобы ее можно было использовать для различных проверок на доступ к ресурсам.
            */
            if(jwtService.isTokenValid(jwt, userDetails)) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        filterChain.doFilter(request, response);
    }
}

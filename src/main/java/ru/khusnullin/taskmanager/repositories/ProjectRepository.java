package ru.khusnullin.taskmanager.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.khusnullin.taskmanager.models.Project;
import ru.khusnullin.taskmanager.models.Task;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findAll();

    Optional<Project> findByTasks(Task task);
}

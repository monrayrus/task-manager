##Управление задачами на Java с использованием Spring Framework
###Описание

Приложение для управления задачами на Java с использованием Spring Framework позволяет создавать, 
назначать, отслеживать и завершать задачи в команде или в личных целях. 
Для создания задач используется форма, в которой можно указать заголовок, описание, 
сроки выполнения и другую информацию. Каждая задача имеет статус, который может быть 
обновлен при изменении состояния задачи. Также приложение позволяет прикреплять файлы, 
комментарии и другую информацию к каждой задаче.

###Функциональность

1. Создание задачи с указанием заголовка, описания, сроков выполнения и другой информации
2. Назначение задачи на конкретного исполнителя
3. Отслеживание статуса задачи (выполнена, в работе, отложена и др.)
4. Завершение задачи с возможностью указания результата
5. Прикрепление файлов, комментариев и другой информации к каждой задаче
##Требования
Для запуска приложения необходимы следующие компоненты:

Java 8 или выше,
Maven,
Браузер

###Установка и запуск
1. Склонируйте репозиторий:
git clone https://gitlab.com/monrayrus/task-manager.git
2. Перейдите в директорию проекта:
cd task-manager
3. Соберите проект с помощью Maven:
mvn clean install
4. Запустите приложение:
java -jar target/task-manager-1.0.jar
5. Откройте веб-браузер и перейдите по адресу:
http://localhost:8080
##Использование
##В приложении доступны следующие функции:

- Регистрация нового пользователя
- Аутентификация пользователя
- Добавление новой задачи
- Просмотр списка задач
- Отметка задачи как выполненной
- Удаление задачи
###Технологии
- Spring Framework
- Spring Boot 2.7.11
- Spring Web
- Spring Data JPA
- H2 Database (Postgres)
###Структура проекта
- task-manager/
  - src/ 
    - main/
      - java/
        - ru.khusnullin.taskmanager/
          - config/
            - SecurityConfig.java
          - controller/ 
             - TaskController.java 
             - UserController.java
          - model/
            - Task.java
            - User.java
            - Role.java
          - repository/
            - UserRepository.java
            - TaskRepository.java
          - service/
            - TaskService.java
            - UserService.java
          - TaskManagerApplication.java
          - WebSecurityConfig.java
      - resources/
        - static/
          - css/
            - style.css
        - templates/
          - add-task.html
          - edit-task.html
          - login.html
          - registration.html
          - task-details.html
          - tasks.html
        - application.properties
    - test/
      - java/
        - ru.khusnullin.taskmanager/
          - controller/
            - TaskControllerTest.java
            - UserControllerTest.java
          - service/
            - TaskServiceTest.java
            - UserServiceTest.java
          - TaskManagerApplicationTests.java



- src/main/java/ru.khusnullin.taskmanager/ - исходный код приложения
- config/ - конфигурация Spring Security
- controller/ - контроллеры для обработки HTTP-запросов
- model/ - модели данных
- repository/ - репозитории для работы с базой данных
- service/ - сервисы для обработки бизнес-логики
- TaskManagerApplication.java - главный класс приложения
- WebSecurityConfig.java - конфигурация Spring Security
- src/main/resources/ - ресурсы приложения
- static/css/ - стили CSS
- templates/ - шаблоны Thymeleaf
- application.properties - настройки приложения
- src/test/java/ru.khusnullin.taskmanager/ - тесты приложения
- controller/ - тесты контроллеров
- service/ - тесты сервисов
- TaskManagerApplicationTests.java - тесты приложения
- pom.xml - файл с настройками проекта для Maven
- README.md - файл с описанием проекта
